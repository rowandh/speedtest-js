var Speedtest = (function($) {

    "use strict";

    /*
     * Run a complete speedtest
     * @param {Object} config
     * @param {Array} [config.downloadUrls] array of download URLs
     * @param {Array} [config.uploadUrls] array of upload URLs
     * @param {Array} [config.downloadProgressHandler] download progress event handler/s
     * @param {Array} [config.uploadProgressHandler] upload progress event handler/s
     * @param {String} [config.messageHandler] message event handler
     */
    var _Speedtest = function (config) {
        this.downloadUrls = config.downloadUrls; // Array of URL objects { url: url, size: size in bytes }
        this.uploadUrls = config.uploadUrls;
        this.downloadProgressHandlers = config.downloadProgressHandlers;
        this.uploadProgressHandlers = config.uploadProgressHandlers;
        this.doneHandlers = config.doneHandlers;
        this.messageHandler = config.messageHandler;
        this.startTime = 0;
        this.endTime = 0;
        this.chunks = 4;
    }

    _Speedtest.prototype.startTest = function () {
        this.startDownloadTest();
    };

    _Speedtest.prototype.startDownloadTest = function () {
        var that = this;

        // This is ugly and complicated

        var test = estimateConnectionSpeed(this.downloadUrls[0].url, this.downloadUrls[0].size)
            .then( // Make a decision based on speed
                function () {
                    return downloadSpeedTest(that.downloadUrls, 4);
                },
                function () {
                    console.log(arguments);
                },
                function (event) {
                    console.log(event);
                })
            .then(
                function () {
                    console.log("Done");
                },
                function () {
                },
                    that.downloadProgressHandlers[0]

            );

        /*
        var chainedTest = estimateConnectionSpeed(this.downloadUrls[0].url, this.downloadUrls[0].size)
            .then( // Make a decision based on speed
                function () {
                    return chainer("http://thunderbird.guardiansd.com/random1000x1000.jpg");
                },
                function () {
                    console.log(arguments);
                },
                function (event) {
                    console.log(event);
                })
            .then(
                function () {
                    console.log(arguments);
                },
                function () {
                },
                function (event) {
                    console.log(event);
                }
        );
        */
    }

    function chainer(url) {
        return downloader(url, 1, 1, 1).then(
            function () {
                return downloader(url, 1, 1, 1);
            }
        );
    }

    _Speedtest.prototype.startUploadTest = function () {
        this.startTime = new Date().getTime();
        return uploader(this.uploadUrls[0], _Speedtest.Util.generateUploadData(1000000), null, null, 1);
    }

    /*
     * Gets file sizes for an array of url objects if object has no size property
     */
    function checkFileSizes (urls) {
        var promises = [];

        // Check if file sizes
        $.each(urls, function (index, downloadUrl) {
            if(!downloadUrl.hasOwnProperty("size")) {
                promises.push( probeFileSize(downloadUrl.url) );
            }
        });

        return $.when.apply( null, promises );
    }

    function estimateConnectionSpeed (url, size) {
        var startTime,
            duration,
            speed;

        startTime = new Date().getTime();
        return splitFileDownloader(url, size, 1)
            .then( function (result) {
                speed = size/((new Date().getTime() - startTime));
                return speed;
            });
    }

    function downloadSpeedTest(urls, duration) {
        var deferred = new $.Deferred();


        var urlFunc = function (urls) {
            var urls = urls,
                called = 0,
                weiter = true;

             var interval = setInterval( function () {
             weiter = false;
                 console.log("Timer expired");
             }, 1000*duration);

            return function () {
                called++;
                if(weiter && urls[called])
                    return urls[called];
                else
                    return false;
            }
        };

        var urlGenerator = urlFunc(urls);

        var download = function download (urlGenerator) {
            var downloadUrl = urlGenerator();

            if(downloadUrl)
                return splitFileDownloader(downloadUrl.url, downloadUrl.size, 1)
                    .then( function () {
                        return download(urlGenerator);
                    });
        }
        return download(urlGenerator);
    }
    /*
     * A function to wrap a split file download into a single deferred and
     * single progress event. Use this and set chunks = 1 instead of calling downloader directly.
     */
    function splitFileDownloader (url, size, chunks) {

        var startByte = 0, endByte = 0,
            deferreds = [],
            startTime,
            chunkSizes,
            result;

        chunkSizes = calculateChunkSizes(size, chunks);

        if (chunkSizes){
            for(var i=0; i < chunks; i++) {
                endByte += chunkSizes[i];
                // Download whole file
                if (chunks === 1) startByte = endByte = chunkSizes [i];
                deferreds[i] = downloader(url, startByte, endByte, i);
                startTime = new Date().getTime();
                startByte = endByte + 1;
            }
        }
        /*
         * Returns a deferred which depends on all chunks downloading
         * Results are accessed in order, eg. for callback handler:
         * function (result_1, result_2, ..., result_n) { ... }
         */
         return $.when.apply( null, deferreds );
    }

    /*
     * Internal downloader function
     * Wrapper for XMLHttpRequest
     * Returns a promise
     */
    function downloader (url, startByte, endByte, id) {
        var deferred = new $.Deferred(),
            lastLoaded = 0,
            increment = 0,
            startTime = 0,
            endTime = 0,
            lastTime = 0,
            thisTime = 0,
            duration = 0,
            currentSpeed,
            instantaneousSpeed;

        if (startByte > endByte) {
            deferred.reject({
                "message" : "error",
                "error" : "startbyte > endbyte"
            });
            return deferred.promise();
        }
        if (window.XMLHttpRequest) {
            var xhr = new XMLHttpRequest();
            url = url + "?" + new Date().getTime(); // Prevent caching

            xhr.addEventListener("progress", function (event) {
                thisTime = new Date().getTime();
                increment = event.loaded - lastLoaded;
                lastLoaded = event.loaded;
                currentSpeed = event.loaded/((thisTime - startTime)/1000);
                instantaneousSpeed = increment/((thisTime - lastTime)/1000);
                lastTime = thisTime;
                deferred.notify({
                    "loaded" : event.loaded,
                    "increment" : increment,
                    "total" : event.total,
                    "average" : currentSpeed,
                    "instantaneous" : instantaneousSpeed,
                    "id" : id,
                    "time" : new Date().getTime()
                });
            }, false);
            xhr.onreadystatechange = function () {
                if(xhr.readyState == 4) {
                    endTime = new Date().getTime();
                    duration = (endTime - startTime)/1000;
                    // 206 is status for partial download
                    if(xhr.status == 206) {
                        deferred.resolve({
                            "message" : "success 206",
                            "id" : id,
                            "data" : xhr.response,
                            "duration" : duration,
                            "time" : new Date().getTime()
                        });
                      }
                 }
            }
            xhr.onload = function (){
                deferred.resolve({
                    "message" : "success 200",
                    "id" : id,
                    "data" : xhr.response,
                    "duration" : duration,
                    "time" : new Date().getTime()
                });
            }
            xhr.onerror = function (){
                deferred.reject({
                    "message" : "error",
                    "error" : "Downloading chunk failed"
                });
            }
            xhr.open('GET', url);
            xhr.responseType = 'blob';

            // Only specify a range if startByte != endByte
            if (startByte !== endByte) {
                xhr.setRequestHeader("Range", "bytes=" + startByte + "-" + endByte);
            }
            startTime = new Date().getTime();
            xhr.send();
        }
        else {
            deferred.reject({
                "message" : "error",
                "error" : "XMLHttpRequest not supported"
            })
        }
        return deferred.promise();
    }

    /*
     * Internal uploader function
     */
    function uploader (url, data, startByte, endByte, id) {
        var lastLoaded = 0,
            increment = 0,
            startTime = 0,
            endTime = 0,
            lastTime = 0,
            thisTime = 0,
            duration = 0,
            currentSpeed,
            instantaneousSpeed;

        var ajax = $.ajax({
            url: url,
            type: "POST",
            data: data,
            xhr: function () {
                var newXhr = $.ajaxSettings.xhr();
                console.log(newXhr);
                if (newXhr) {
                    newXhr.upload.addEventListener("progress", function(progressEvent) {
                           ajax.notify( function () {
                                return "Test"
                            });
                        }, false);
                }

                return newXhr;
                }
        }).progress(function (event) {
            console.log("Progress");
        });

        return ajax;
    }

    /*
     * Function to ping a given URL
     * URL should be a file of a small number of bytes
     * Returns a promise which then returns the ping time in milliseconds
     */
    function ping (url) {
        var startTime,
            duration,
            ping;

        startTime = new Date().getTime();
        return downloader(url, 1, 1, "ping")
            .then( function (result) {
                ping = (new Date().getTime() - startTime);
                return ping;
            });
    }

    /*
     * Probe a remote file size
     * @param {String} url - remote file URL
     * Returns a promise
     */
    function probeFileSize (url) {

        var length = 0,
            loaded = 0,
            deferred = new $.Deferred();

        if (window.XMLHttpRequest) {
            var xhr = new XMLHttpRequest();

            xhr.addEventListener("progress", function (event) {
               if (event.lengthComputable) {
                   // Get the total length of the file.
                   // Safest way is to wait for event.lengthComputable
                   length = event.total;
                   loaded = event.loaded;
                   xhr.abort();
               }
            });
            xhr.onabort = function () {
                if (typeof(length) === "number" && length > 0) {
                    deferred.resolve({
                      "message" : "success",
                      "url" : url,
                      "length" : length,
                      "loaded" : loaded
                    });
                }
                else {
                    deferred.reject({
                        "message" : "error",
                        "error" : "Unable to get remote file size"
                    });
                }
            }
            xhr.onload = function (){
                deferred.reject({
                    "message" : "error",
                    "error" : "File loaded. This shouldn't have happened."
                });
            }
            xhr.onerror = function (){
                deferred.reject({
                    "message" : "error",
                    "error" : "XHR onerror triggered."
                });
            }
            xhr.open('GET', url);
            xhr.send();
        }
        return deferred.promise();
    }

    /*
     * Chunk size calculator
     * @param {Number} length - Length of file
     * @param {Number} chunks - Number of chunks to create
     */
    function calculateChunkSizes (length, chunks) {

        var residue = length % chunks,
            chunkSizes = [];

        // If there is a remainder, subtract it from the length
        // Otherwise, residue is zero and this does nothing
        length -= residue;
        for (var i = 0; i < chunks; i++) {
            chunkSizes[i] = length/chunks;
        }
        chunkSizes[chunks - 1] += residue;
        return chunkSizes;
    }

    /*
     * Generates random data of (size) bytes
     */
    function generateUploadData (size) {

        var uploadInfo = {},
            chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz",
            randomstring = '',
            rnum,
            string_length = (size > 8000000) ?  8000000 : size; // 8mb server limit

        for (var i=0; i < string_length; i++) {
                rnum = Math.floor(Math.random() * chars.length);
                randomstring += chars.charAt(rnum);
        }
        uploadInfo.data = randomstring;
        uploadInfo.size = randomstring.length; //Bytes

        return uploadInfo;
    };

    return _Speedtest;

})(jQuery)