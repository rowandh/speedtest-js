(function($, Speedtest) {
    $(document).ready( function () {

       var downloadProgressHandlers = [],
           doneHandlers = [],
           results = [],
           outputBlob,
           totalLoaded = 0,
           currentProgress = 0,
           startTime,
           chunks = 4;
       
       for (var i = 0 ; i < chunks; i++) {
           $('#progressbars').append("<div><h3>Thread " + i + "</h3><div id=\'progressbar-"+ i + "\'></div><span id=\'progressspeed-"+ i + "\'></span></div>");
           
           downloadProgressHandlers.push(function (result) {
                        if(result) {
                            totalLoaded += result.increment;
                            currentProgress = (totalLoaded/result.total*100/chunks);
                            $('#progressbar-'+result.id).progressbar({value: (result.loaded/result.total*100)});
                            $('#progressspeed-'+result.id).html(result.average/1000 + " Kb/s");
                            $('#totalprogress').progressbar({value: currentProgress});
                            $('#totalspeed').html(totalLoaded/(1000*(new Date().getTime() - startTime)/1000) + " Kb/s");
                        }
                    });
       }
       
       var config = {
                downloadUrls: [{url: "http://thunderbird.guardiansd.com/dog_70kb.jpg", size: 71734},
                               {url: "http://thunderbird.guardiansd.com/random500x500.jpg", size: 505544},
                               {url: "http://thunderbird.guardiansd.com/random1000x1000.jpg", size: 1986284},
                               {url: "http://thunderbird.guardiansd.com/random1500x1500.jpg", size: 4468241},
                               {url: "http://thunderbird.guardiansd.com/random2000x2000.jpg", size: 7907740}],
                uploadUrls: ["http://thunderbird.guardiansd.com/upload.php"],
                downloadProgressHandlers: downloadProgressHandlers,
                uploadProgressHandlers: function () {},
                messageHandler: function (message) {
                    console.log(message);
                },
                doneHandlers: doneHandlers
           };

       this.speedtest = new Speedtest(config);
       
       startTime = new Date().getTime();
       /*
       $.when(speedtest.startDownloadTest()).done( function () {
           console.log(arguments);
           for(var i = 0; i < arguments.length; i++) {
               results[arguments[i].id] = arguments[i].data;
           }
           outputBlob = new Blob(results, {type : 'image/jpeg'});
           $('#result').html("<img src='" + window.URL.createObjectURL(outputBlob) + "'>");                
       });
       */
        this.speedtest.startDownloadTest();
       //$.when(speedtest.startUploadTest()).done( function () {
       //    console.log("arguments");
       //});

    });
})(jQuery, Speedtest);